# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from django.contrib.auth.models import User
from django.contrib.auth.admin import UserAdmin
from .models import Addresses, UserProfile, MembershipPlan

class AddressesTabularInline(admin.TabularInline):
    model = Addresses
    extra = 2

class ProfileInline(admin.StackedInline):
    model = UserProfile
    extra = 1

class UserProfileAdmin(UserAdmin):
    inlines = [AddressesTabularInline, ProfileInline]

class MembershipPlanAdmin(admin.ModelAdmin):
    list_display = ["title", "price", "active"]
    list_filter = ["active"]
    list_editable = ["active"]

admin.site.register(Addresses)
admin.site.unregister(User)
admin.site.register(User, UserProfileAdmin)
admin.site.register(MembershipPlan, MembershipPlanAdmin)
