# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone
from inventory import choices

class Addresses(models.Model):
    user = models.ForeignKey(User)
    line1 = models.CharField(max_length=150, blank=True, null=True)
    line2 = models.CharField(max_length=150, blank=True, null=True)
    city = models.CharField(max_length=150, blank=True, null=True)
    state = models.CharField(max_length=150, blank=True, null=True)
    country = models.CharField(max_length=50, default="India")
    zipcode = models.IntegerField(blank=True, null=True)
    latitude = models.FloatField(blank=True, null=True)
    longitude = models.FloatField(blank=True, null=True)

class MembershipPlan(models.Model):
    title = models.CharField(max_length=150)
    description = models.TextField(blank=True, null=True)
    price = models.FloatField(default=0.00)
    tax_percentage = models.FloatField(default=0.00)
    active = models.BooleanField(choices=choices.YESNO, default=False)

    def __str__(self):
        return self.title

class UserProfile(models.Model):
    user = models.OneToOneField(User)
    dob = models.DateField(blank=True, null=True)
    dp = models.FileField(max_length=150, blank=True, null=True, upload_to="user-dps", default="default.jpg")
    is_chef = models.BooleanField(choices=choices.YESNO, default=False)
    about = models.TextField(blank=True, null=True)
    addresses = models.ManyToManyField(Addresses)
    location = models.CharField(max_length=150)
    membership_plan = models.ForeignKey(MembershipPlan, blank=True, null=True)

    def __str__(self):
        return user.pk
