YESNO = (
    (True, "Yes"),
    (False, "No")
)

MEALPLAN_FREQUENCY = (
    ("Daily", "Daily"),
    ("Weekly", "Weekly"),
    ("Bi-Weekly", "Bi-Weekly"),
    ("Fortnightly", "Fortnightly"),
    ("Monthly", "Monthly"),
)
