# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import FoodItem, MealPlans

class FoodItemAdmin(admin.ModelAdmin):
    list_fields = ["title", "price", "in_stock", "allotted", "chef"]
    search_fields = ["title", "chef__first_name"]
    list_filter = ["in_stock"]
    readonly_fields = ["allotted"]

class MealPlansAdmin(admin.ModelAdmin):
    list_fields = ["title", "price", "in_stock", "allotted", "chef"]
    search_fields = ["title", "chef__first_name"]
    list_filter = ["in_stock"]
    readonly_fields = ["allotted"]


admin.site.register(FoodItem, FoodItemAdmin)
admin.site.register(MealPlans, MealPlansAdmin)
