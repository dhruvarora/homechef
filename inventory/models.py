# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.utils import timezone
from . import choices
from django.contrib.auth.models import User as HomeChef

class FoodItem(models.Model):
    date_added = models.DateTimeField(default=timezone.now)
    chef = models.ForeignKey(HomeChef)
    title = models.CharField(max_length=150)
    price = models.FloatField()
    description = models.TextField(blank=True, null=True)
    in_stock = models.BooleanField(choices=choices.YESNO, default=False)
    allotted = models.IntegerField(default=0)

    def __str__(self):
        return self.title


class MealPlans(models.Model):
    date_added = models.DateTimeField(default=timezone.now)
    chef = models.ForeignKey(HomeChef)
    title = models.CharField(max_length=150)
    frequency = models.CharField(choices=choices.MEALPLAN_FREQUENCY, default=True, null=True, max_length=150)
    price = models.FloatField()
    description = models.TextField()
    in_stock = models.BooleanField(choices=choices.YESNO, default=False)
    allotted = models.IntegerField(default=0)

    def __str__(self):
        return self.title
