# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from .models import Order, Transaction

class OrderAdmin(admin.ModelAdmin):
    list_display = ["pk", "order_total", "date_added"]
    search_fields = ["pk", "customer__last_name", "customer__first_name", "chef__first_name", "customer__last_name"]

admin.site.register(Order, OrderAdmin)
admin.site.register(Transaction)
