# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
from inventory.models import FoodItem

class Order(models.Model):
    date_added = models.DateTimeField(default=timezone.now)
    chef = models.ForeignKey(User, related_name="chef")
    customer = models.ForeignKey(User, related_name="customer")
    items = models.ManyToManyField(FoodItem)
    order_total = models.FloatField(default=0.00)

    def __str__(self):
        return self.pk


class Transaction(models.Model):
    date_added = models.DateTimeField(default=timezone.now)
    order = models.ForeignKey(Order, blank=True, null=True)
    value = models.FloatField(default=0.00)
    mode = models.CharField(max_length=150, default="Payment Gateway")
    transaction_id = models.CharField(max_length=150, null=True, blank=True)
    comments = models.CharField(max_length=150, blank=True, null=True)

    def __str__(self):
        return self.pk + " " + self.comments + " transaction id: " + self.transaction_id
