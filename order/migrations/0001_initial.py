# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-12-05 11:55
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('inventory', '0003_auto_20171204_1219'),
    ]

    operations = [
        migrations.CreateModel(
            name='Order',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_added', models.DateTimeField(default=django.utils.timezone.now)),
                ('order_total', models.FloatField(default=0.0)),
                ('chef', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='chef', to=settings.AUTH_USER_MODEL)),
                ('customer', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='cusstomer', to=settings.AUTH_USER_MODEL)),
                ('items', models.ManyToManyField(to='inventory.FoodItem')),
            ],
        ),
        migrations.CreateModel(
            name='Transaction',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_added', models.DateTimeField(default=django.utils.timezone.now)),
                ('value', models.FloatField(default=0.0)),
                ('mode', models.CharField(default='Payment Gateway', max_length=150)),
                ('transaction_id', models.CharField(blank=True, max_length=150, null=True)),
                ('comments', models.CharField(blank=True, max_length=150, null=True)),
                ('order', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='order.Order')),
            ],
        ),
    ]
